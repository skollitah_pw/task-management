package pl.edu.pw.okno.ti.taskmanagement.interfaces.rest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Component
@ControllerAdvice
public class ExceptionMappingConfig {

    @ResponseBody
    @ResponseStatus(value = NOT_FOUND)
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorDto> handle(IllegalArgumentException exception) {
        return new ResponseEntity<>(
                new ErrorDto(exception.getMessage()),
                NOT_FOUND);
    }

    @ResponseBody
    @ResponseStatus(value = BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDto> handle(MethodArgumentNotValidException exception) {
        return new ResponseEntity<>(
                new ErrorDto(String.join(", ", exception.getBindingResult().getFieldErrors().toString())),
                BAD_REQUEST);
    }

    @Data
    private static class ErrorDto {

        private final String message;
    }
}
