package pl.edu.pw.okno.ti.taskmanagement.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DateHelper {

    public static Instant now() {
        return LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant();
    }
}
