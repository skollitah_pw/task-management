package pl.edu.pw.okno.ti.taskmanagement.infrastructure.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.pw.okno.ti.taskmanagement.domain.ProjectRepository;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Project;

public interface ProjectRepositorySpringData extends ProjectRepository, JpaRepository<Project, String> {
}
