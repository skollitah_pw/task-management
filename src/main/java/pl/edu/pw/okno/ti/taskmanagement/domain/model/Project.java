package pl.edu.pw.okno.ti.taskmanagement.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.edu.pw.okno.ti.taskmanagement.util.DateHelper;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Getter
public class Project {

    @Id
    private String id;

    private String name;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant lastModifiedAt;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "project_id")
    private List<Task> tasks = new ArrayList<>();

    public Project(String name, List<Task> tasks) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.tasks = tasks;
        this.createdAt = DateHelper.now();
        this.lastModifiedAt = DateHelper.now();
    }

    public void addTask(Task task) {
        tasks.add(task);
        this.lastModifiedAt = DateHelper.now();
    }

    public void update(String name) {
        this.name = name;
        this.lastModifiedAt = DateHelper.now();
    }
}
