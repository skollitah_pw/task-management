package pl.edu.pw.okno.ti.taskmanagement.interfaces.rest;

import org.springframework.stereotype.Component;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Project;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Task;

import java.util.ArrayList;

@Component
class FormMapper {

    Task map(TaskForm taskForm) {
        return new Task(taskForm.getName(), taskForm.getDetails());
    }

    Project map(ProjectForm projectForm) {
        return new Project(projectForm.getName(), new ArrayList<>());
    }
}
