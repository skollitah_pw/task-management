package pl.edu.pw.okno.ti.taskmanagement.domain;

import pl.edu.pw.okno.ti.taskmanagement.domain.model.Task;

import java.util.Optional;

public interface TaskRepository {

    Optional<Task> findById(String id);

    boolean existsById(String id);

    Task save(Task task);

    void deleteById(String id);
}
