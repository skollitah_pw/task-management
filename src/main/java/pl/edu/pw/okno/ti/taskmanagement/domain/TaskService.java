package pl.edu.pw.okno.ti.taskmanagement.domain;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Task;

@Component
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    public Task getTask(String taskId) {
        Preconditions.checkArgument(taskId != null, "taskId is null");

        return findTaskById(taskId);
    }

    public void deleteTask(String taskId) {
        Preconditions.checkArgument(taskId != null, "taskId is null");

        if (!taskRepository.existsById(taskId)) {
            throw new IllegalArgumentException(
                    String.format("Task with id: [%s] not found", taskId));
        }

        taskRepository.deleteById(taskId);
    }

    public Task updateTask(String taskId, String name, String details) {
        Preconditions.checkArgument(taskId != null, "taskId is null");

        Task task = findTaskById(taskId);
        task.update(name, details);

        return taskRepository.save(task);
    }

    public Task markCompleted(String taskId) {
        Preconditions.checkArgument(taskId != null, "taskId is null");
        Task task = findTaskById(taskId);
        task.complete();
        return taskRepository.save(task);
    }

    public Task markInProgress(String taskId) {
        Preconditions.checkArgument(taskId != null, "taskId is null");
        Task task = findTaskById(taskId);
        task.inProgress();
        return taskRepository.save(task);
    }

    public Task markOnHold(String taskId) {
        Preconditions.checkArgument(taskId != null, "taskId is null");
        Task task = findTaskById(taskId);
        task.onHold();
        return taskRepository.save(task);
    }

    public Task markToDo(String taskId) {
        Preconditions.checkArgument(taskId != null, "taskId is null");
        Task task = findTaskById(taskId);
        task.toDo();
        return taskRepository.save(task);
    }

    private Task findTaskById(String taskId) {
        return taskRepository.findById(taskId)
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("Task with id: [%s] not found", taskId)));
    }
}
