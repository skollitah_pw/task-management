package pl.edu.pw.okno.ti.taskmanagement.infrastructure.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.pw.okno.ti.taskmanagement.domain.TaskRepository;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Task;

public interface TaskRepositorySpringData extends TaskRepository, JpaRepository<Task, String> {
}
