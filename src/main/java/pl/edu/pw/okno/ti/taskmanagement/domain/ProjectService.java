package pl.edu.pw.okno.ti.taskmanagement.domain;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Project;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Task;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectRepository projectRepository;

    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    public Project getProject(String projectId) {
        Preconditions.checkArgument(projectId != null, "projectId is null");

        return findProjectById(projectId);
    }

    public Task addTask(String projectId, Task task) {
        Preconditions.checkArgument(projectId != null, "projectId is null");
        Preconditions.checkArgument(task != null, "task is null");

        Project project = projectRepository.findAll().stream()
                .filter(it -> projectId.equals(it.getId()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("Project with id: [%s] not found", projectId)));

        project.addTask(task);
        projectRepository.save(project);

        return task;
    }

    public Project addProject(Project project) {
        Preconditions.checkArgument(project != null, "project is null");

        return projectRepository.save(project);
    }

    public Project updateProject(String projectId, String name) {
        Preconditions.checkArgument(projectId != null, "projectId is null");

        Project project = findProjectById(projectId);
        project.update(name);

        return projectRepository.save(project);
    }

    public void deleteProject(String projectId) {
        Preconditions.checkArgument(projectId != null, "projectId is null");

        if (!projectRepository.existsById(projectId)) {
            throw new IllegalArgumentException(
                    String.format("Project with id: [%s] not found", projectId));
        }

        projectRepository.deleteById(projectId);
    }

    private Project findProjectById(String projectId) {
        return projectRepository.findById(projectId)
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("Project with id: [%s] not found", projectId)));
    }
}
