package pl.edu.pw.okno.ti.taskmanagement.domain.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.edu.pw.okno.ti.taskmanagement.util.DateHelper;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.time.Instant;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
public class Task {

    @Id
    private String id;

    private String name;

    @Column(length = 2000)
    private String details;

    @Enumerated(EnumType.STRING)
    private Status status;

    private Instant createdAt;

    private Instant lastModifiedAt;

    public Task(String name, String details) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.details = details;
        this.status = Status.TO_DO;
        this.createdAt = DateHelper.now();
        this.lastModifiedAt = DateHelper.now();
    }

    public void complete() {
        this.status = Status.COMPLETED;
        this.lastModifiedAt = DateHelper.now();
    }

    public void inProgress() {
        this.status = Status.IN_PROGRESS;
        this.lastModifiedAt = DateHelper.now();
    }

    public void onHold() {
        this.status = Status.HOLD;
        this.lastModifiedAt = DateHelper.now();
    }

    public void toDo() {
        this.status = Status.TO_DO;
        this.lastModifiedAt = DateHelper.now();
    }

    public void update(String name, String details) {
        this.name = name;
        this.details = details;
        this.lastModifiedAt = DateHelper.now();
    }

    @AllArgsConstructor
    @Getter
    public enum Status {
        TO_DO("To Do"),
        IN_PROGRESS("In Progress"),
        COMPLETED("Completed"),
        HOLD("Hold");

        @JsonValue
        private String label;
    }
}
