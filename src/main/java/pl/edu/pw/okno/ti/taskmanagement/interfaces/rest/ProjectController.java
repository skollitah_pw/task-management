package pl.edu.pw.okno.ti.taskmanagement.interfaces.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pw.okno.ti.taskmanagement.domain.ProjectService;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Project;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Task;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/projects")
public class ProjectController {

    private final ProjectService projectService;
    private final FormMapper formMapper;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get all projects",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Projects list fetched successfully"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public List<Project> getAllProjects() {
        return projectService.getAllProjects();
    }

    @GetMapping(value = "/{project_id}",
            produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get project",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Project fetched successfully"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public Project getProject(
            @PathVariable("project_id")
            @ApiParam(name = "project_id",
                    value = "Project id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String projectId) {

        return projectService.getProject(projectId);
    }

    @PostMapping(value = "/{project_id}/tasks",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create new task for project",
            httpMethod = "POST",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public ResponseEntity<Task> addTask(
            @PathVariable("project_id")
            @ApiParam(name = "project_id",
                    value = "Project id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String projectId,
            @ApiParam(name = "taskForm",
                    value = "Task data",
                    required = true)
            @Valid
            @RequestBody TaskForm taskForm) {

        Task task = projectService.addTask(projectId, formMapper.map(taskForm));

        return ResponseEntity.status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, String.format("/tasks/%s", task.getId()))
                .body(task);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create new empty project",
            httpMethod = "POST",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public ResponseEntity<Project> addProject(
            @ApiParam(name = "projectForm",
                    value = "Task data",
                    required = true)
            @Valid
            @RequestBody ProjectForm projectForm) {

        Project project = projectService.addProject(formMapper.map(projectForm));

        return ResponseEntity.status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, String.format("/projects/%s", project.getId()))
                .body(project);
    }

    @DeleteMapping(value = "/{project_id}")
    @ApiOperation(value = "Delete project", httpMethod = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Project deleted successfully"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public ResponseEntity<Void> delete(
            @PathVariable("project_id")
            @ApiParam(name = "project_id",
                    value = "Project id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String projectId) {

        projectService.deleteProject(projectId);

        return ResponseEntity.noContent().build();
    }

    @PutMapping(value = "/{project_id}",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update project",
            httpMethod = "PUT",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public Project updateProject(
            @PathVariable("project_id")
            @ApiParam(name = "project_id",
                    value = "Project id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String projectId,
            @ApiParam(name = "projectForm",
                    value = "Form data",
                    required = true)
            @Valid
            @RequestBody ProjectForm projectForm) {

        return projectService.updateProject(projectId, projectForm.getName());
    }
}
