package pl.edu.pw.okno.ti.taskmanagement.infrastructure.persistance;

import com.github.javafaker.Faker;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Project;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Task;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class ProjectMock {

    private static Faker faker = new Faker();
    private static List<Supplier<Task>> taskSuppliers = Arrays.asList(
            ProjectMock::mockGameOfThrones,
            ProjectMock::mockChuckNorris,
            ProjectMock::mockDune,
            ProjectMock::mockWitcher,
            ProjectMock::mockJoda,
            ProjectMock::mockBook,
            ProjectMock::mockLorem);

    static List<Project> mockProjects(int maxProjects, int maxTasksForProject) {
        return Stream.generate(new ProjectSupplier(maxTasksForProject))
                .limit(1 + new Random().nextInt(maxProjects))
                .collect(Collectors.toList());
    }

    private static Task mockGameOfThrones() {
        return mockTaskStatus(new Task(
                faker.gameOfThrones().character(),
                faker.gameOfThrones().quote()));
    }

    private static Task mockChuckNorris() {
        return mockTaskStatus(new Task(
                faker.funnyName().name(),
                faker.chuckNorris().fact()));
    }

    private static Task mockDune() {
        return mockTaskStatus(new Task(
                faker.dune().planet(),
                faker.dune().quote()));
    }

    private static Task mockJoda() {
        return mockTaskStatus(new Task(
                faker.space().constellation(),
                faker.yoda().quote()));
    }

    private static Task mockWitcher() {
        return mockTaskStatus(new Task(
                faker.witcher().monster(),
                faker.witcher().quote()));
    }

    private static Task mockBook() {
        return mockTaskStatus(new Task(
                faker.book().title(),
                faker.shakespeare().asYouLikeItQuote()));
    }

    private static Task mockLorem() {
        return mockTaskStatus(new Task(
                faker.lorem().sentence(10),
                faker.lorem().sentence(50)));
    }

    private static Task mockTaskStatus(Task task) {
        List<Runnable> statusModifiers = Arrays.asList(
                task::complete,
                task::inProgress,
                task::onHold,
                task::toDo);

        statusModifiers.get(new Random().nextInt(statusModifiers.size())).run();

        return task;
    }

    @RequiredArgsConstructor
    private static class ProjectSupplier implements Supplier<Project> {

        private final int maxTasksForProject;

        @Override
        public Project get() {
            List<Task> tasks = Stream.generate(taskSuppliers.get(new Random().nextInt(taskSuppliers.size())))
                    .limit(new Random().nextInt(maxTasksForProject))
                    .collect(Collectors.toList());

            return new Project(faker.company().buzzword(), tasks);
        }
    }
}
