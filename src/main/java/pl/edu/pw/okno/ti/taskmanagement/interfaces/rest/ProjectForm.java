package pl.edu.pw.okno.ti.taskmanagement.interfaces.rest;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class ProjectForm {

    @NotBlank
    @ApiModelProperty(required = true, value = "Project name", example = "Task management")
    private String name;
}
