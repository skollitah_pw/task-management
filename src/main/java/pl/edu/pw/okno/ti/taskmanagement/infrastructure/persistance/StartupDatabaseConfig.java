package pl.edu.pw.okno.ti.taskmanagement.infrastructure.persistance;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.pw.okno.ti.taskmanagement.domain.ProjectRepository;

@Configuration
public class StartupDatabaseConfig {

    @Bean
    StartupDatabaseInitializer startupDatabaseInitializer(
            @Value("${app.mock.max-projects}") int maxProjects,
            @Value("${app.mock.max-tasks-for-project}") int maxTasksForProject,
            ProjectRepository repository) {

        return new StartupDatabaseInitializer(repository, maxProjects, maxTasksForProject);
    }
}
