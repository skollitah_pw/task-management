package pl.edu.pw.okno.ti.taskmanagement.infrastructure.persistance;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import pl.edu.pw.okno.ti.taskmanagement.domain.ProjectRepository;

@Slf4j
@RequiredArgsConstructor
public class StartupDatabaseInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private final ProjectRepository repository;
    private final int maxProjects;
    private final int maxTasksForProject;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("Application started");

        ProjectMock.mockProjects(maxProjects, maxTasksForProject).forEach(repository::save);
    }
}
