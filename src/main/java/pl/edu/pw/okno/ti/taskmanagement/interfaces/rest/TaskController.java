package pl.edu.pw.okno.ti.taskmanagement.interfaces.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pw.okno.ti.taskmanagement.domain.TaskService;
import pl.edu.pw.okno.ti.taskmanagement.domain.model.Task;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    @GetMapping(value = "/{task_id}",
            produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get task",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Task fetched successfully"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public Task getTask(
            @PathVariable("task_id")
            @ApiParam(name = "task_id",
                    value = "Task id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String taskId) {

        return taskService.getTask(taskId);
    }

    @DeleteMapping(value = "/{task_id}")
    @ApiOperation(value = "Delete task", httpMethod = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Task deleted successfully"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public ResponseEntity<Void> delete(
            @PathVariable("task_id")
            @ApiParam(name = "task_id",
                    value = "Task id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String taskId) {

        taskService.deleteTask(taskId);

        return ResponseEntity.noContent().build();
    }

    @PatchMapping(value = "/{task_id}/complete", produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mark task as completed",
            httpMethod = "PATCH",
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Task marked successfully"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public Task markCompleted(
            @PathVariable("task_id")
            @ApiParam(name = "task_id",
                    value = "Task id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String taskId) {

        return taskService.markCompleted(taskId);
    }

    @PatchMapping(value = "/{task_id}/to_do", produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mark task as to be done",
            httpMethod = "PATCH",
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Task marked successfully"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public Task markToDo(
            @PathVariable("task_id")
            @ApiParam(name = "task_id",
                    value = "Task id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String taskId) {

        return taskService.markToDo(taskId);
    }

    @PatchMapping(value = "/{task_id}/in_progress", produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mark task as in progress",
            httpMethod = "PATCH",
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Task marked successfully"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public Task markInProgress(
            @PathVariable("task_id")
            @ApiParam(name = "task_id",
                    value = "Task id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String taskId) {

        return taskService.markInProgress(taskId);
    }

    @PatchMapping(value = "/{task_id}/on_hold", produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mark task as in progress",
            httpMethod = "PATCH",
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Task marked successfully"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public Task markOnHold(
            @PathVariable("task_id")
            @ApiParam(name = "task_id",
                    value = "Task id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String taskId) {

        return taskService.markOnHold(taskId);
    }

    @PutMapping(value = "/{task_id}",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update task",
            httpMethod = "PUT",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Server failure")
    })
    public Task updateProject(
            @PathVariable("task_id")
            @ApiParam(name = "task_id",
                    value = "Task id as UUID",
                    example = "5035c8ad-43aa-4c20-8a06-6954ca41caed",
                    required = true) String taskId,
            @ApiParam(name = "taskForm",
                    value = "Form data",
                    required = true)
            @Valid
            @RequestBody TaskForm taskForm) {

        return taskService.updateTask(taskId, taskForm.getName(), taskForm.getDetails());
    }
}
