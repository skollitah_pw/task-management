FROM maven:3.6.3-jdk-8 AS maven-build
WORKDIR /build
COPY . ./
RUN mvn clean package

FROM openjdk:8u242-jre-slim
COPY --from=maven-build /build/target/task-management-0.0.1-SNAPSHOT.jar /usr/app/task-management-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/app/task-management-0.0.1-SNAPSHOT.jar"]