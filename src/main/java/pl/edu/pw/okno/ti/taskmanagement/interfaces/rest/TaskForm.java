package pl.edu.pw.okno.ti.taskmanagement.interfaces.rest;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class TaskForm {

    @NotBlank
    @ApiModelProperty(required = true, value = "Task name", example = "General information")
    private String name;

    @NotBlank
    @ApiModelProperty(required = true, value = "Task details", example = "Specific description, what to do")
    private String details;
}
