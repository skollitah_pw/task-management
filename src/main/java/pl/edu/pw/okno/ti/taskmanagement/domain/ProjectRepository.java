package pl.edu.pw.okno.ti.taskmanagement.domain;

import pl.edu.pw.okno.ti.taskmanagement.domain.model.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository {

    List<Project> findAll();

    Optional<Project> findById(String id);

    boolean existsById(String id);

    Project save(Project project);

    void deleteById(String id);
}
